from django.shortcuts import render,redirect
from django.contrib import auth
from .forms import UserCreationForm


def register(request):
    if request.method == 'POST':
        user_form = UserCreationForm(request.POST)
        if user_form.is_valid():
            #create new user object not save
            new_user = user_form.save(commit = False)
            #set choosen password
            new_user.set_password(user_form.cleaned_data['password2'])
            #save user
            new_user.save()
            return render(request, 'accounts/register_done.html',
            {'new_user':
            new_user})
        else:
            user_form = UserCreationForm()
            return render(request, 'accounts/register.html',
                          {'user_form': user_form,'error':'Invalid'})
    else:
        user_form = UserCreationForm()
        return render(request, 'accounts/register.html',
                {'user_form':user_form})




def login(request):
    if request.method == 'POST':
        user = auth.authenticate(username=request.POST['email'], password = request.POST['password'])
        if user is not None:
            auth.login(request, user)
            return redirect('home')
        else:
            return render(request, 'accounts/login.html', {'error':'Not Valid'})
    else:
        return render(request, 'accounts/login.html')

def logout(request):
    """
    if request.method == 'POST'
        auth wala done !!!!
    """
    auth.logout(request)
    return redirect('home')