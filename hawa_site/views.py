from django.shortcuts import render
from django.core.paginator import Paginator, EmptyPage,\
PageNotAnInteger

def home_page(request):
    return render(request, 'home.html')